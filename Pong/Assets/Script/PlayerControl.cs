﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    //Var fiska
    private ContactPoint2D lastContactPoint;

    public ContactPoint2D LastContactPoint
    {
        get
        {
            return lastContactPoint;
        }
    }

    //mengambil kontak
    private void OnCollisionEnter2D(Collision2D collision)
    {
        lastContactPoint = collision.GetContact(0);    
    }

    //Tombol utk Menggerakkan ke Atas
    public KeyCode UpBtn = KeyCode.W;

    //TOmbol utk menggerakkan ke bawah
    public KeyCode DownBtn = KeyCode.S;

    //Kecepatan
    public float speed = 10.0f;

    //Batas Kamera/Scene
    public float yBoudary = 9.0f;

    //RigidBody2D
    private Rigidbody2D rgb;

    //Score
    public int score;

    // Start is called before the first frame update
    void Start()
    {
        rgb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //Mendapatkan Kecepatan Raket Sekarang
        Vector2 velocity = rgb.velocity;

        //press btnUp maka ke atas
        if (Input.GetKey(UpBtn))
        {
            velocity.y = speed;
        }
        //press btnDown maka ke bawah
        else if (Input.GetKey(DownBtn))
        {
            velocity.y = -speed;
        }
        else
            velocity.y = 0.0f;

        //memasukkan kembali kecepatan ke rgb
        rgb.velocity = velocity;

        //Posisi Raket
        Vector3 pos = transform.position;

        //jika melewati batas, kembalikan ke batas tsb
        //atas
        if(pos.y > yBoudary)
        {
            pos.y = yBoudary;
        }
        //bawah
        else if (pos.y < -yBoudary)
        {
            pos.y = -yBoudary;
        }

        //masukkan kembali posisi ke transform
        transform.position = pos; 
    }
    //Score
    public void MenambahScroe()
    {
        score++;
    }

    //reset Score
    public void ResetScore()
    {
        score = 0;
    }

    //mendapatkan nilai Score
    public int Score
    {
        get
        {
            return score;
        }
    }
}
