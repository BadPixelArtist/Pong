﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallControl : MonoBehaviour
{
    //var titik lintasan asal
    private Vector2 trajectoryOrigin;

    //Rigidbody Bola
    private Rigidbody2D rgb;

    //Besar Gaya Awal Bola
    public float xInitialForce;
    public float yIntiialForce;

    //methodeReset Bola
    void ResetBall()
    {
        transform.position = Vector2.zero;

        //mereset kecepatan
        rgb.velocity = Vector2.zero;
    }

    void PushBall()
    {
        float yRandomInitialForce = Random.Range(-yIntiialForce, yIntiialForce);

        float randomDirection = Random.Range(0, 2);

        if(randomDirection < 1.0f)
        {
            rgb.AddForce(new Vector2(-xInitialForce, yRandomInitialForce));
        }else
        {
            rgb.AddForce(new Vector2(-xInitialForce, -yRandomInitialForce));
        }
    }

    void RestartGame()
    {
        ResetBall();

        Invoke("PushBall", 2);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        trajectoryOrigin = transform.position;
    }

    public Vector2 TrajectoryOrigin
    {
        get
        {
            return trajectoryOrigin;
        }
    }
    private void Start()
    {
        trajectoryOrigin = transform.position;

        rgb = GetComponent<Rigidbody2D>();

        RestartGame();
    }

    void Update()
    {
                
    }
}
