﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trajectory : MonoBehaviour
{
    //Var rigid, colli
    public BallControl ball;
    CircleCollider2D ballCollider;
    Rigidbody2D ballRgb;

    //Bayangan Bola
    public GameObject ballTubrukan;

    //bool
    bool drawBallTubrukan = false;

    //titik TUmbukan yg digeser
    Vector2 offsetHitPoint = new Vector2();


    // Start is called before the first frame update
    void Start()
    {
        ballRgb = ball.GetComponent<Rigidbody2D>();
        ballCollider = ball.GetComponent<CircleCollider2D>();

    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D[] circleCastHit2DArray =
        Physics2D.CircleCastAll(ballRgb.position, ballCollider.radius,
        ballRgb.velocity.normalized);

        foreach (RaycastHit2D circleCastHit2D in circleCastHit2DArray)
        {
            if (circleCastHit2D.collider != null &&
                circleCastHit2D.collider.GetComponent<BallControl>() == null)
            {
                Vector2 hitPoint = circleCastHit2D.point;

                //titik normal tumbukan
                Vector2 hitNormal = circleCastHit2D.normal;

                offsetHitPoint = hitPoint + hitNormal * ballCollider.radius;

                //gambar garis
                DottedLine.DottedLine.Instance.DrawDottedLine(ball.transform.position, offsetHitPoint);

                if (circleCastHit2D.collider.GetComponent<WallSide>() == null)
                {
                    // Hitung vektor datang
                    Vector2 inVector = (offsetHitPoint - ball.TrajectoryOrigin).normalized;

                    // Hitung vektor keluar
                    Vector2 outVector = Vector2.Reflect(inVector, hitNormal);

                    // Hitung dot product dari outVector dan hitNormal. Digunakan supaya garis lintasan ketika 
                    // terjadi tumbukan tidak digambar.
                    float outDot = Vector2.Dot(outVector, hitNormal);
                    if (outDot > -1.0f && outDot < 1.0)
                    {
                        // Gambar lintasan pantulannya
                        DottedLine.DottedLine.Instance.DrawDottedLine(
                            offsetHitPoint,
                            offsetHitPoint + outVector * 10.0f);

                        // Untuk menggambar bola "bayangan" di prediksi titik tumbukan
                        drawBallTubrukan = true;
                    }
                }
                break;
            }
                // Jika true, ...
                if (drawBallTubrukan)
                {
                    // Gambar bola "bayangan" di prediksi titik tumbukan
                    ballTubrukan.transform.position = offsetHitPoint;
                    ballTubrukan.SetActive(true);
                }
                else
                {
                    // Sembunyikan bola "bayangan"
                    ballTubrukan.SetActive(false);
                }
            }


            // Hanya gambar lintasan untuk satu titik tumbukan, jadi keluar dari loop
           

        }
    }


