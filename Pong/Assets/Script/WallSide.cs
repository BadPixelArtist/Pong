﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallSide : MonoBehaviour
{
    [SerializeField] private GameManager gameManager;
    public PlayerControl player;

    private void OnTriggerEnter2D(Collider2D anotherCollider)
    {
        if(anotherCollider.name == "ball")
        {
            player.MenambahScroe();

            if(player.Score < gameManager.maxScore)
            {
                anotherCollider.gameObject.SendMessage("RestartGame", 2.0f, SendMessageOptions.RequireReceiver);
            }
        }
    }
}
