﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    //var pemain
    public PlayerControl player1, player2;
    private Rigidbody2D player1Rgb, player2Rgb;

    //var bola
    public BallControl ball;
    private Rigidbody2D ballRgb;
    private CircleCollider2D ballCollider;

    //var skor
    public int maxScore;


    private bool isDebugWindowsShown = false;

    // Objek untuk menggambar prediksi lintasan bola
    public Trajectory trajectory;


    private void OnGUI()
    {
        //player10), "" + player1.Score);
        //player2
        GUI.Label(new Rect(Screen.width / 2 - 150 - 12, 20, 100, 100), "" + player1.Score);
        GUI.Label(new Rect(Screen.width / 2 + 150 + 12, 20, 100, 100), "" + player2.Score);

        //tombolRestart
        if (GUI.Button(new Rect(Screen.width / 2 - 60, 35, 120, 53), "RESTART"))
        {
            //jika btnRestart ditekan, reset socre pemain
            player1.ResetScore();
            player2.ResetScore();

            //bola direstart.
            ball.SendMessage("RestartGame", 0.5f, SendMessageOptions.RequireReceiver);
        }
        if (player1.Score == maxScore)
        {
            GUI.Label(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 10, 2000, 1000)
                , "PLAYER ONE WINS");
        }
        else if (player2.Score == maxScore)
        {
            GUI.Label(new Rect(Screen.width / 2 + 30, Screen.height / 2 - 10, 2000, 1000)
               , "PLAYER TWO WINS");
        }
    if (isDebugWindowsShown)
        {
            trajectory.enabled = !trajectory.enabled;
           
            GUI.backgroundColor = Color.red;

            // Simpan variabel-variabel fisika yang akan ditampilkan. 
            float ballMass = ballRgb.mass;
            Vector2 ballVelocity = ballRgb.velocity;
            float ballSpeed = ballRgb.velocity.magnitude;
            Vector2 ballMomentum = ballMass * ballVelocity;
            float ballFriction = ballCollider.friction;

            float impulsePlayer1X = player1.LastContactPoint.normalImpulse;
            float impulsePlayer1Y = player1.LastContactPoint.tangentImpulse;
            float impulsePlayer2X = player2.LastContactPoint.normalImpulse;
            float impulsePlayer2Y = player2.LastContactPoint.tangentImpulse;

            // Tentukan debug text-nya
            string debugText =
                "Ball mass = " + ballMass + "\n" +
                "Ball velocity = " + ballVelocity + "\n" +
                "Ball speed = " + ballSpeed + "\n" +
                "Ball momentum = " + ballMomentum + "\n" +
                "Ball friction = " + ballFriction + "\n" +
                "Last impulse from player 1 = (" + impulsePlayer1X + ", " + impulsePlayer1Y + ")\n" +
                "Last impulse from player 2 = (" + impulsePlayer2X + ", " + impulsePlayer2Y + ")\n";

            // Tampilkan debug window
            GUIStyle guiStyle = new GUIStyle(GUI.skin.textArea);
            guiStyle.alignment = TextAnchor.UpperCenter;
            GUI.TextArea(new Rect(Screen.width / 2 - 200, Screen.height - 200, 400, 110), debugText, guiStyle);

            Color oldColor = GUI.backgroundColor;

        }
        // Toggle nilai debug window ketika pemain mengeklik tombol ini.
        if (GUI.Button(new Rect(Screen.width / 2 - 60, Screen.height - 73, 120, 53), "TOGGLE\nDEBUG INFO"))
        {
            isDebugWindowsShown = !isDebugWindowsShown;
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        player1Rgb = GetComponent<Rigidbody2D>();
        player2Rgb = GetComponent<Rigidbody2D>();
        ballRgb = ball.GetComponent<Rigidbody2D>();
        ballCollider = ball.GetComponent<CircleCollider2D>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
